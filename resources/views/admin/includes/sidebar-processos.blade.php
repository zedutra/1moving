<style type="text/css">
    .nav-link a{
        color: #898B96 !important;
    }
    .nav-link a:hover{
        color: #FF733A !important;
    }
</style>
<div class="siderbar">
    <ul class="nav nav-pills nav-sidebar flex-column">
        <li class="nav-item">
            <a href="#" class="nav-link">Busca</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Geral</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Carga</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Documentos / Seguro</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">OS</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Embalagem</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Madeira</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Faturamento</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Custos</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Cálculo</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Financiamento</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">FUP/NPS</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Fiscal</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Resultado</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">Margem</a>
        </li>
    </ul>
</div>