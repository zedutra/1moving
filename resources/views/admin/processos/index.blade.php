@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="d-flex justify-content-between">
        <div class="p-2">
            <h1>Processos</h1>
        </div>
        <div class="p-2">
            <button class="btn btn-md btn-outline-primary mr-4" type="button">Novo</button>
            <button class="btn btn-md btn-primary" type="button">Salvar</button>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop

@section('content')
    <div class="container-fluid" style="background-color: white;">
        <div class="row">
            <div class="col-md-2" style="background-color: #15223214;">
                @include('admin.includes.sidebar-processos')
            </div>
            <div class="col-md-10 p-4">
                <form action="">
                    <div class="p-4">
                        <h3>Dados da Mudança</h3>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label for="pergunta">Código</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Ano</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Tipo</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Modal</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Status</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label for="pergunta">Booking</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Canal</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-6">
                            <label for="pergunta">Agente</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label for="pergunta">Ref#</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Provider</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Cadastro</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Alt. Status</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-3">
                            <label for="coord">Concluído (Coord.)</label>
                            <input type="checkbox" name="coord" id="coord">
                        </div>
                        <div class="col-3">
                            <label for="geral">Concluído (Geral)</label>
                            <input type="checkbox" name="geral" id="geral">
                        </div>
                        <div class="col-3">
                            <input type="date" name="data" id="data">
                        </div>
                    </div>

                    <div class="p-4">
                        <h3>Dados do Cliente</h3>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <label for="pergunta">Cliente</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-5">
                            <label for="pergunta">Empresa</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <label for="pergunta">E-mail</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-5">
                            <label for="pergunta">Visto</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label for="pergunta">Vendedor</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Vistoriador</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Coordenador</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label for="pergunta">Processo Relacionado</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Estágio</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                    </div>

                    <div class="p-4">
                        <h3>Endereço de Origem</h3>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label for="pergunta">CEP</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-8">
                            <label for="pergunta">Endereço</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Número</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label for="pergunta">Complemento</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Bairro</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Cidade</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Estado</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label for="pergunta">País</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Telefone</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-6">
                            <label for="pergunta">Agente</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>

                    <div class="p-4">
                        <h3>Endereço de Destino</h3>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label for="pergunta">CEP</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-8">
                            <label for="pergunta">Endereço</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Número</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2">
                            <label for="pergunta">Complemento</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Bairro</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-4">
                            <label for="pergunta">Cidade</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-2">
                            <label for="pergunta">Estado</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label for="pergunta">País</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-3">
                            <label for="pergunta">Telefone</label>
                            <input type="number" class="form-control"  name="nome" value="">
                        </div>
                        <div class="col-6">
                            <label for="pergunta">Agente</label>
                            <input type="text" class="form-control"  name="nome" value="">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-between p-4">
        <div class="p-2">
        </div>
        <div class="p-2">
            <button class="btn btn-md btn-outline-primary mr-4" type="button">Novo</button>
            <button class="btn btn-md btn-primary" type="button">Salvar</button>
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop